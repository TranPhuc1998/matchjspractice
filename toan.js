const { zhCN } = require("date-fns/locale");

function getMost(input) {
  let total = {};
  for (let i = 0; i < input.length; i++) {
    if (!total[input.charAt(i)]) {
      total[input.charAt(i)] = 1;
    } else {
      total[input.charAt(i)]++;
    }
  }

  let max = { value: 0, text: "" };
  for (let i in total) {
    if (total[i] > max.value) {
      max.value = total[i];
      max.text = i.toString();
    }
  }
  return max.text;
}

// console.log(getMost("hello"));

function runningSum(nums) {
  for (let index = nums.length - 1; index > 0; index--) {
    let sum = 0;
    for (let previous = index - 1; previous >= 0; previous--) {
      sum += nums[previous];
    }
    nums[index] += sum;
  }
  return nums;
}

console.log(runningSum([1, 2, 3, 4]));

var runningSum = function (nums) {
  let val = 0;
  let sum = nums.map((res) => {
    return (val = res + val);
  });
  return sum;
};

console.log(runningSum([1, 2, 3, 4, 5, 6]));

var removeDuplicates = function (nums) {
  const length = nums.length;
  let j = 0;

  for (let i = 0; i < length; i++) {
    if (i < length - 1 && nums[i] == nums[i + 1]) {
      continue;
    }
    console.log((nums[j++] = nums[i]));
  }

  return j;
};

console.log(removeDuplicates([1, 1, 1, 3, 5, 7]));

// function removeDuplicates(array) {
//   return array.filter((a, b) => {
//     if (array.length > 0) {
//       array.indexOf(a) === b;
//     }
//   });
// }

// console.log(removeDuplicates([0, 0, 1, 1, 1, 2, 2, 3, 3, 4, 5, 1]), "gello");
